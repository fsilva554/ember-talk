class ReviewController < ApplicationController

  def create
    review = Review.create(permitted_params_review)
    
    render json: { review: review }
  end

  private

    def permitted_params_review
      params.require(:review).permit(:title, :text, :product_id)
    end

end
