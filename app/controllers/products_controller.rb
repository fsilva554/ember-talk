class ProductsController < ApplicationController

  def index
    render json: Product.all
  end

  def show
    render json: Product.find(params[:product_id])
  end

  def create
    product = Product.create(permitted_params)
    render json: { product: product }
  end

  private
    def permitted_params
      params.require(:product).permit(:name, :code, :price, :description, :stock)
    end

end
