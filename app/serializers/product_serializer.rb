class ProductSerializer < ActiveModel::Serializer
  embed :ids, include: true

  attributes :id, :name, :code, :price, :description, :stock
  has_many :reviews
end
