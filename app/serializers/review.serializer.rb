class ReviewSerializer < ActiveModel::Serializer
  embed :ids, include: true

  attributes :id, :text
  belongs_to :product
end
