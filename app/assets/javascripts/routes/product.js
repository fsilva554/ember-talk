App.ProductsProductRoute = Ember.Route.extend({

	model: function(params){
		return this.store.find('product', params.product_id);
	},

	deactivate: function(){

		controller = this.get('controller');
		
		controller.set('title', '');
		controller.set('text', '');

	}

});
