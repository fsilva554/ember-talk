App.ProductsNewRoute = Ember.Route.extend({

  model: function(){
    return this.store.createRecord('product');
  },

  actions: {

    willTransition: function(transition){

      controller = this.get('controller');

      if (controller.get('isDirty') &&
          !confirm("Are you sure you want to abandon progress?")) {
        transition.abort();
        if (window.history) {
          window.history.forward();
        }
        return false;
      } else {
        controller.get("content").rollback();
        return true;
      }
    }
  }
});
