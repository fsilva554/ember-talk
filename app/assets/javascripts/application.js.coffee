#= require jquery
#= require handlebars
#= require ember
#= require ember-data
#= require_self
#= require ember_talk

# for more details see: http://emberjs.com/guides/application/
window.EmberTalk = Ember.Application.create()

