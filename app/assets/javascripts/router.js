
App.Router.map(function(){

	this.route('index', { path: '/' });
	this.resource('products', function(){
		this.resource('products.product', { path: '/:product_id' });
		this.route('new');
	});

});


