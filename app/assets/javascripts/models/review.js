App.Review = DS.Model.extend({
  title: DS.attr('string'),
  text: DS.attr('string'),
  product: DS.belongsTo('product', { async: true })
});
