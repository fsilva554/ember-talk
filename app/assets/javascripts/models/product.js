App.Product = DS.Model.extend({
  name: DS.attr('string'),
  description: DS.attr('string'),
  code: DS.attr('string'),
  price: DS.attr('number'),
  stock: DS.attr('number'),
  reviews: DS.hasMany('review', { async: true }),
  isLowOnStock: Ember.computed.lte('stock', 10)
});
