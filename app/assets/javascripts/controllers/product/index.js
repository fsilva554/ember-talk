App.ProductsIndexController = Ember.ArrayController.extend({
	productsCount: Ember.computed.alias('length'),

  isLowOnStock: Ember.computed.lte('model.@each.stock', 10),

  filter: '',

  sortProperties: [],
  sortedProds: Ember.computed.sort('filteredContent', 'sortProperties'),

  filteredContent: function(){
    var filter = this.get('filter');
    var rx = new RegExp(filter, 'gi');

    var products = this.get('model');

    return products.filter(function(p) {
      
      if(p.get('isNew'))
        return false
      else
        return p.get('name').match(rx) || p.get('code').match(rx);
    });

  }.property('model.@each.id', 'filter'),

	actions: {
    sortBy: function(property) {
      var orientation = this.get('sortAscending') == true ? ':asc' : ':desc';

      this.set('sortProperties', [property + orientation]);

      this.set('sortAscending', !this.get('sortAscending'));
    }
	}

});
