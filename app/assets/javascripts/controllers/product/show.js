App.ProductsProductController = Ember.ObjectController.extend({

  actions: {
    createReview: function() {
      var controller = this;

      this.store.createRecord('review', {
        product: this.get('model'),
        text: this.get('text'),
        title: this.get('title')
      }).save().then(function(review){
        controller.set('text', '');
        controller.set('title', '');
      });
      
    }
  }

})
