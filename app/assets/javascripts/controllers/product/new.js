App.ProductsNewController = Ember.ObjectController.extend({
  
  actions: {

    save: function(){

      var self = this;

      this.get('model').save().then(function(response){
        self.transitionToRoute('products');
      });

    }

  }

})
