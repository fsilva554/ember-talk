class AddFieldsToProducts < ActiveRecord::Migration
  def change
    add_column :products, :description, :string
    add_column :products, :stock, :integer
  end
end
